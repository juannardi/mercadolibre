package nardi.francisco.juan.testnardi;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;


public class CategoriasActivity extends AppCompatActivity {

    private static final String TAG = "CategoriasActivity";
    Adapter_Categorias adapter;
    ListView listView;
    String categoria_madre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categorias);
        ActionBar actionBar = this.getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("mercado libre");
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setLogo(R.mipmap.ic_launcher_ml);
        categoria_madre = null;
        if(savedInstanceState != null && savedInstanceState.containsKey("categoria_madre")) {
            categoria_madre = savedInstanceState.getString("categoria_madre");
            populateListView(categoria_madre);
        }else{
            populateListView("");
        }
        //Cargo el listview
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_sin_busqueda, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.compartir:
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, "Código de evaluación. Candidato: Nardi Juan Francisco" +
                        System.getProperty("line.separator") + "Has probado esta App? .Apk disponible en " + getResources().getResourceName(R.string.link_repo));

                startActivity(Intent.createChooser(shareIntent, "Has probado esta App? .Apk disponible en " + getResources().getResourceName(R.string.link_repo)));
                return true;
            case R.id.salir:
                this.finish();
                return true;
            case R.id.contactarse:
                final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                emailIntent.setType("plain/text");
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{getResources().getResourceName(R.string.email)});
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Test App ML");
                startActivity(Intent.createChooser(emailIntent, "Envía un e-mail..."));
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void populateListView(String tags) {

        adapter = new Adapter_Categorias(CategoriasActivity.this, tags);
        listView = (ListView) findViewById(R.id.lista_categorias);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //En el onclick, tomo el dato del id_categoria y cargo la listview con ese dato como padre
                //Por el tiempo restringido no se implementó una solución para navegar hacia adelanto y atrás
                //de las opciones ya seleccionadas
                //Cuando no encuentra mas categorías hijas, se dirije a la actividad principal y muestra los productos de esa categoría
                Item_Categoria it_cat = (Item_Categoria) adapter.getItem(position);

                if (it_cat != null) {
                    categoria_madre = it_cat.getId();
                    populateListView(it_cat.getId());
                }
            }

        });
    }
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt("position_c", listView.getFirstVisiblePosition());
        savedInstanceState.putString("categoria_madre", categoria_madre);
        //Me guardo la posición del listview antes de que cambie el estado de la pantalla (previniendo el cambio al girar el dispositivo por ejemplo)
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {

        try {
            super.onRestoreInstanceState(savedInstanceState);
            //al restaurarse la actividad, consulto si la listview se encontraba en alguna posición de scroll en particular y le indico que la muestre así
            final int position = savedInstanceState.getInt("position_c");
            listView.clearFocus();
            listView.post(new Runnable() {
                @Override
                public void run() {
                    //infelizmente, el comando setselection no funciona si no es a travéz de un hilo aparte
                    //desarrollé una prueba utilizando un Handler con un recyclerview, pero por una cuestión de tiempos acotados
                    //voy a optar por esta solución que pasó las pruebas realizadas (Dejo el adaptador por cualquier consulta).
                    listView.setSelection(position);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Error de parsing: " + e.getMessage());
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        adapter.limpiar_dialogos();
    }
}
